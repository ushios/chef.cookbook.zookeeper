#
# Cookbook Name:: zookeeper
# Recipe:: install
#
# Copyright 2013, UshioShugo<ushio.s@gmail.com>
#
# All rights reserved - Do Not Redistribute
#

package 'zookeeperd' do
	action :install
end