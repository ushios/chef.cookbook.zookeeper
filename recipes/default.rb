#
# Cookbook Name:: zookeeper
# Recipe:: default
#
# Copyright 2013, UshioShugo<ushio.s@gmail.com>
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'zookeeper::install'